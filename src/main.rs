use {
    i3ipc::{I3EventListener, Subscription},
    i3ipc::event::{Event, WindowEventInfo},
    i3ipc::event::inner::WindowChange,
    i3ipc::reply::Node,
    std::process::Command,
    std::thread,
    std::time::Duration,
};

fn main() {
    // establish connection.
    let mut listener = I3EventListener::connect().unwrap();

    // subscribe to a couple events.
    let subs = [Subscription::Window];
    listener.subscribe(&subs).unwrap();

    // handle them
    for event in listener.listen() {
        match event.unwrap() {
            Event::WindowEvent(e) => handle_event(e),
            //_ => unreachable!()
            _ => ()
        }
    }
}

fn handle_event(event: WindowEventInfo) {
    match event.change {
        WindowChange::Urgent => handle_window(event.container),
        _ => (),
    }
}

fn handle_window(window: Node) {
    match window.window {
        Some(id) => urgent_timer(id),
        None => println!("No current window"),
    }
}

fn urgent_timer(id: i32) {
    println!("{}", id);
    let delay = Duration::from_secs(10);
    let window = id.to_string();

    thread::sleep(delay);

    Command::new("wmctrl")
            .args(["-i", "-r", &window, "-b", "remove,demands_attention"])
            .output()
            .expect("failed to execute wmctrl");
}
